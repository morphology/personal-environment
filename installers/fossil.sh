#! /bin/sh
set -eu
install_prefix="${HOME}"
source_dir=$( realpath "src/${package_name}-src" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/fossil
cd build/fossil
  $source_dir/configure \
    --prefix="$install_prefix" \
    --json
  
  make -j -l $(nproc)
  install -D -s fossil "${install_prefix}/bin/fossil"
cd -
