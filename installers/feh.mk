TMPDIR := $(shell mktemp -d)

%/src/${package_name} :
	git clone -b "${VERSION}" https://git.finalrewind.org/${package_name} "$@"
