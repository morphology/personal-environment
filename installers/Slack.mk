#! /usr/bin/make -rf
VERSION ?= 4.34.120
KEY_VERSION ?= 20230710
include _defaults.mk

@install : ${DOWNLOAD_DIR}/slack-desktop-${VERSION}-amd64.deb
	${SUDO} apt install "$<"

@key : ${DOWNLOAD_DIR}/slack_pubkey_20230710.gpg
	wget -P "$(@D)" "https://slack.com/gpg/$(@F)"

${DOWNLOAD_DIR}/slack-desktop-%.deb : | @key
	wget -P "$(@D)" \
		"https://downloads.slack-edge.com/releases/linux/${VERSION}/prod/x64/$(@F)"
	gpgv "$@"

${DOWNLOAD_DIR}/slack_%.gpg :
	wget -P "$(@D)" "https://slack.com/gpg/$(@F)"

# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
