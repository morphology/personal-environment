#! /bin/sh
package_name=age
anitya_project_id=142630
set -e

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi

make -rf "${package_name}.mk" ${install_prefix}/bin/age ${install_prefix}/bin/age-plugin-yubikey
