#! /bin/sh
package_name=nodejs
anitya_project_id=8251  # See also nodejs~lts
# See https://docs.github.com/en/copilot/getting-started-with-github-copilot?tool=vimneovim#prerequisites-3
VERSION=17.9.1
set -e

. _hooks/setup.sh
source_dir="/usr/local/src/node-v${VERSION}"
. ${hooks_dir}/pre_install.sh

# --with-intl=system-icu: use the system version of icu.
#
# To use system icu, a fairly new version must be available Other values are
# full-icu (to build a local, full icu library) and small-icu (to build
# a local, minimal icu library).

cd "$source_dir"
	$source_dir/configure \
		--prefix="$install_prefix" \
		--ninja \
		--shared-libuv \
		--shared-nghttp2 \
		--shared-openssl \
		--shared-zlib \
		--with-intl=small-icu
	nice make -j -l $(nproc)
	make install
cd -
. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
	*/home/*) . ${hooks_dir}/post_install.sh ;;
	*)
		. ${hooks_dir}/post_root_install.sh
		$SUDO ldconfig
		;;
esac
node -e "console.log('Hello from Node.js ' + process.version)"
# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix 
