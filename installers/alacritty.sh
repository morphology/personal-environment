#! /bin/sh
package_name=alacritty
anitya_project_id=98436
set -e

. _hooks/setup.sh
source_dir=$( realpath ~/src/${package_name} )
. ${hooks_dir}/pre_install.sh

cargo install --path "$source_dir/$package_name"
if ! gzip "$source_dir/extra"/*.man; then
	echo "Failed to compress man pages"
fi >&2

install -d "${install_prefix}/bin/" "${install_prefix}/share/man/man1"
install -D -s ~/.cargo/bin/alacritty "$install_prefix/bin/"
install -m 0644 -C "${source_dir}/extra/logo/alacritty-term.svg" "${install_prefix}/bin/"
install -D "${source_dir}/extra"/*.man.gz "${install_prefix}/share/man/man1"
install -C -D "${source_dir}/extra/completions/alacritty.bash" \
	"/usr/share/bash-completion/completions/alacritty"
if ! infocmp alacritty > /dev/null; then
	$SUDO tic -xe alacritty,alacritty-direct "${source_dir}/extra/alacritty.info"
fi
. ${hooks_dir}/post_root_install.sh
$SUDO update-alternatives --install \
	$(which x-terminal-emulator) x-terminal-emulator \
	"${install_prefix}/bin/${package_name}" 39

# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
