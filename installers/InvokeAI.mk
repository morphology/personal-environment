#! /usr/bin/make -rf
VERSION ?= 2.2.4
PATCH_VERSION ?= 2.2.4-p1

TMPDIR := $(shell mktemp -d)

${TMPDIR}/InvokeAI-Installer : ${TMPDIR}/InvokeAI-installer-${PATCH_VERSION}-linux.zip
	cd "$(@D)" ; unzip "$<"

${TMPDIR}/InvokeAI-%.zip :
	curl -fL -o "$@" "https://github.com/invoke-ai/InvokeAI/releases/download/v${VERSION}/$(@F)"
