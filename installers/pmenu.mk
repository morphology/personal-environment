VERSION ?= 2.5.0

TMPDIR := $(shell mktemp -d)

%/src/${package_name} :
	mkdir -p "$(@D)"
	git clone --depth 1 -b "v${VERSION}" "git@github.com:phillbush/$(@F).git" "$@"

%/share/openmoji : ${TMPDIR}/openmoji-72x72-color.zip
	unzip -d "$@" "$<"

${TMPDIR}/openmoji-% :
	curl -fL -o "$@" "https://github.com/hfg-gmuend/openmoji/releases/download/13.1.0/$(@F)"
