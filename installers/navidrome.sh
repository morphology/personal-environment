#! /bin/sh
anitya_project_id=326646
package_name=navidrome
set -e

# Requires ffmpeg

. _hooks/setup.sh
export SUDO

if ! command -v ffmpeg; then
  echo "Install ffmpeg first"
  exit 1
fi >&2

getent group Music || $SUDO groupadd Music
$SUDO install -d -g Music -m 0755 /home/Music
$SUDO install -d -o navidrome -g staff -m 0775 /var/lib/navidrome

getent passwd navidrome || $SUDO useradd -r \
  -g Music -d "/var/lib/navidrome" -s /bin/false \
  navidrome

make -rf "${package_name}.mk" /opt/navidrome/ /var/lib/navidrome/navidrome.toml /usr/local/lib/systemd/system/${package_name}.service
