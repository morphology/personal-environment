#! /usr/bin/env make -rf
TMPDIR := $(shell mktemp -d)

build/acroread : ${TMPDIR}/AdbeRdr9.5.5-1_i386linux_enu.deb
	mkdir -p "$@"
	cp "$<" "$@"

${TMPDIR}/AdbeRdr% :
	wget -O "$@" "ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/$(@F)"
