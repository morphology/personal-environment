TMPDIR := $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/bash-% :
	curl -fL -o "$@"     "http://ftpmirror.gnu.org/gnu/bash/$(@F)" \
	         -o "$@.sig" "http://ftpmirror.gnu.org/gnu/bash/$(@F).sig"
	-gpg --verify "$@.sig"
