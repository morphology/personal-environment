#! /bin/sh
package_name=wireguard
set -eu

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi

mkdir -p "${HOME}/etc/wireguard"
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "${HOME}/etc/wireguard/publickey"
fi
