#! /usr/bin/make -rf
include _defaults.mk
package_name = 1password
arch ?= amd64

@install : @bin /etc/bash_completion.d/${package_name}

@bin : /etc/apt/sources.list.d/${package_name}.list /etc/debsig/policies/AC2D62742012EA22/1password.pol /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg /usr/share/keyrings/1password-archive-keyring.gpg
	apt-get update
	apt-get install 1password 1password-cli

%/${package_name}.list : /etc/apt/keyrings/1password-archive-keyring.gpg
	$(file > $@,deb [arch=${arch} signed-by=$<] https://downloads.1password.com/linux/debian/${arch} stable main)

%/keyrings/1password-archive-keyring.gpg %/keyrings/AC2D62742012EA22/debsig.gpg :
	mkdir -d "$(@D)"
	wget "https://downloads.1password.com/linux/keys/1password.asc"
	gpg --no-default-keyring --keyring "$@" --import "1password.asc"

%/${package_name}.pol :
	install -d "$(@D)"
	wget -O "$@" "https://downloads.1password.com/linux/debian/debsig/$(@F)"

%/completions/${package_name} %/bash_completion.d/${package_name} : | @bin
	install -d "$(@D)"
	op completion bash > "$@"
	bash -n "$@"
