#! /usr/bin/make -rf
package_name = vim-scimark

include _defaults.mk

@install : ${vim_native_plugins}/start/${package_name}
	-${SUDO} apt-get install --no-install-suggests --no-install-recommends $(file < ${package_name}.deps)


${vim_native_plugins}/start/${package_name} :
	mkdir -p "$(@D)"
	git clone --depth=1 "https://github.com/mipmip/$(@F)" "$@"
