import argparse
import http.client
import json
import sys
from contextlib import closing


class BadStatus(Exception):
    pass


class AnityaClient:
    def __init__(self):
        self.conn = http.client.HTTPSConnection("release-monitoring.org")

    def close(self):
        self.conn.close()

    def get_package_data(self, anitya_project_id):
        url_path = f"/api/v2/versions/?project_id={anitya_project_id}"
        self.conn.request("GET", url_path)
        response = self.conn.getresponse()
        if http.client.responses[response.status] == "OK":
            return json.load(response)
        else:
            response.read()  # clears the response
            raise BadStatus(response.reason)

    def get_latest_version(self, anitya_project_id):
        return self.get_package_data(anitya_project_id=anitya_project_id)[
            "latest_version"
        ]

    def get_latest_stable_version(self, anitya_project_id):
        return self.get_package_data(anitya_project_id=anitya_project_id)[
            "stable_versions"
        ][0]


def get_versions(kind: str, project_ids: dict):
    with closing(AnityaClient()) as client:
        match kind:
            case "latest":
                return {
                    name: client.get_latest_version(pid)
                    for name, pid in project_ids.items()
                }
            case "latest_stable":
                return {
                    name: client.get_latest_stable_version(pid)
                    for name, pid in project_ids.items()
                }
            case _:
                raise NotImplementedError(kind)


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("kind", choices=("latest", "latest_stable"))
    parser.add_argument("project_ids", nargs="+")
    args = vars(parser.parse_args())
    project_ids = args.pop("project_ids")
    match len(project_ids):
        case 1:
            (version,) = get_versions(args["kind"], {None: project_ids.pop()}).values()
            print(version)
        case _:
            project_dict = dict(zip(project_ids[::2], project_ids[1::2]))
            for name, version in get_versions(args["kind"], project_dict).items():
                print(f"{name}\t{version}")


if __name__ == "__main__":
    sys.exit(main())
