TMPDIR := $(shell mktemp -d)

%/src/${package_name} :
	git clone --depth=1 -b "v${VERSION}" "https://github.com/muennich/${package_name}.git" "$@"
