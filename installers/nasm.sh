#! /bin/sh -eu
package_name=nasm
VERSION=2.15.05  # Maybe needed by ffmpeg?
anitya_project_id=2048

. _hooks/setup.sh
. ${hooks_dir}/pre_install.sh

mkdir -p ~/build/${package_name}
cd ~/build/${package_name}
  ${source_dir}/configure \
    --prefix="$install_prefix"
  make -j -l $(nproc)
  make install
cd -

case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *) . ${hooks_dir}/post_root_install.sh ;;
esac
