#! /usr/bin/make -rf

.SECONDARY :

@install : /etc/apt/sources.list.d/signal-xenial.list
	apt update
	apt install --reinstall signal-desktop

%/keyrings/signal-desktop-keyring.gpg :
	wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > "$@"

%/sources.list.d/signal-xenial.list : /etc/apt/keyrings/signal-desktop-keyring.gpg
	$(file > $@,deb [arch=amd64 signed-by=$<] https://updates.signal.org/desktop/apt xenial main)
